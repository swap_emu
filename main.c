/*

swap by cth103, jeuxjeux20 and jba03

*/
#include <stdio.h>
#include <SDL2/SDL.h>
#define SCREEN_WIDTH 854
#define SCREEN_HEIGHT 480
int main(int argc, char **argv) {
	SDL_Log("\nswap by cth103, jeuxjeux20 and jba03\ndata taken from reswitched, switchbrew and creatable's switch waiting till im getting mine");
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
    	return 1;
  	}
	window = SDL_CreateWindow(
			    "swap_window",
			    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			    SCREEN_WIDTH, SCREEN_HEIGHT,
			    SDL_WINDOW_SHOWN
			    );
  	if (window == NULL) {
    	fprintf(stderr, "could not create window: %s\n", SDL_GetError());
    	return 1;
  	}
	SDL_Delay(2000);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}