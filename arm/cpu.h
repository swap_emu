struct cpu_t
{
    uint64_t N; // Negative
    uint64_t Z; // Zero
    uint64_t C; // Carry (or Unsigned Overflow)
    uint64_t V; // Overflow (Signed)

    uint64_t PC; // Program Counter
    uint64_t *MEMORY; // Memory
}

void interpret(uint64_t opcode);
