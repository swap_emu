/* CPU Status flags */
uint64_t N; //Negative
uint64_t Z; //Zero
uint64_t C; //Carry (or Unsigned Overflow)
uint64_t V; //Overflow (Signed)
uint64_t opcode;
uint64_t PC; //Program Counter
int isRunning;
uint64_t *mem; //Memory

void start_cpu(); //Start emulating the CPU
void stop_cpu(); //Stop emulating the CPU
void interpret(uint64_t opcode); /* emulate opcode */