/* Opcode flags.  */
#define FLAG_ALIAS (1 << 0)
#define FLAG_HAS_ALIAS (1 << 1)
#define FLAG_P1 (1 << 2)
#define FLAG_P2 (2 << 2)
#define FLAG_P3 (3 << 2)
#define FLAG_COND (1 << 4)
#define FLAG_SF (1 << 5)
#define FLAG_SIZEQ (1 << 6)
#define FLAG_FPTYPE (1 << 7)
#define FLAG_SSIZE (1 << 8)
#define FLAG_T (1 << 9)
#define FLAG_GPRSIZE_IN_Q (1 << 10)
#define FLAG_LDS_SIZE (1 << 11)
#define FLAG_OPD0_OPT (1 << 12)
#define FLAG_OPD1_OPT (2 << 12)
#define FLAG_OPD2_OPT (3 << 12)
#define FLAG_OPD3_OPT (4 << 12)
#define FLAG_OPD4_OPT (5 << 12)
#define FLAG_DEFAULT(X) (((X) & 0x1f) << 15)
#define FLAG_CONV (1 << 20)
#define FLAG_PSEUDO (1 << 21)
#define FLAG_MISC (1 << 22)
#define FLAG_N (1 << 23)
#define FLAG_OD(X) (((X) & 0x7) << 24)
#define FLAG_LSE_SZ (1 << 27)
#define FLAG_STRICT (1ULL << 28)
